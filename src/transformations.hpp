/******************************************************************************
 * Этот файл является частью проекта Assistant Space AutoPilot (ASAP).
 *
 * Проект Assistant Space AutoPilot (ASAP) распространяется согласно положениям
 * базовой модификации Омской открытой лицензии с указанием авторства.
 *
 * Copyright (c) 2023, Андрей Степанов <standmit@yandex.ru>
******************************************************************************/


/**
 * \file        src/transformations.hpp
 * \author      Андрей Степанов <standmit@yandex.ru>
 * \copyright   Омская открытая лицензия
 * \brief       Модуль вспомогательных функция преобразования координат
*/


#pragma once


#include <eigen3/Eigen/Eigen>


namespace ASAP {


/**
 * \brief   Преобразовать углы в кватернион
 * \tparam T    Тип данных для расчётов (float или double)
 * \param roll      Угол поворота вокруг оси X (радианы)
 * \param pitch     Угол поворота вокруг оси Y (радианы)
 * \param yaw       Угол поворота вокруг оси Z (радианы)
*/
template <typename TQ, typename TA>
Eigen::Quaternion<TQ> makeQuat(
        const TA roll,
        const TA pitch,
        const TA yaw
) {
    return static_cast<Eigen::Quaternion<TQ>>(
        Eigen::AngleAxis<TQ>(yaw, Eigen::Matrix<TQ, 3, 1>::UnitZ()) *
        Eigen::AngleAxis<TQ>(pitch, Eigen::Matrix<TQ, 3, 1>::UnitY()) *
        Eigen::AngleAxis<TQ>(roll, Eigen::Matrix<TQ, 3, 1>::UnitX())
    );
}


}