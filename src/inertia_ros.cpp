/******************************************************************************
 * Этот файл является частью проекта Assistant Space AutoPilot (ASAP).
 *
 * Проект Assistant Space AutoPilot (ASAP) распространяется согласно положениям
 * базовой модификации Омской открытой лицензии с указанием авторства.
 *
 * Copyright (c) 2023, Андрей Степанов <standmit@yandex.ru>
******************************************************************************/


/**
 * \file        src/inertia_ros.cpp
 * \author      Андрей Степанов <standmit@yandex.ru>
 * \copyright   Омская открытая лицензия
 * \brief       Нода пересчёта ускорения и силы
*/


#include <ros/ros.h>
#include "inertia.hpp"
#include <geometry_msgs/AccelStamped.h>
#include <geometry_msgs/WrenchStamped.h>


ros::Publisher pub;
const ASAP::Inertia<double>* pInertia;


void accelCallback(const geometry_msgs::AccelStamped& accel) {
    geometry_msgs::WrenchStamped msg;
    msg.header = accel.header;
    {
        double force[3];
        pInertia->getForce(
            std::array<double, 3>{
                accel.accel.linear.x,
                accel.accel.linear.y,
                accel.accel.linear.z
            }.data(),
            force
        );
        msg.wrench.force.x = force[0];
        msg.wrench.force.y = force[1];
        msg.wrench.force.z = force[2];
    }
    {
        double torque[3];
        pInertia->getTorque(
            std::array<double, 3>{
                accel.accel.angular.x,
                accel.accel.angular.y,
                accel.accel.angular.z
            }.data(),
            torque
        );
        msg.wrench.torque.x = torque[0];
        msg.wrench.torque.y = torque[1];
        msg.wrench.torque.z = torque[2];
    }
    pub.publish(msg);
}


int main(int argc, char** argv) {
    ros::init(argc, argv, "inertia");
    ros::NodeHandle nh;
    ros::NodeHandle pnh("~");

    const ASAP::Inertia<double> inertia(
        pnh.param<std::vector<double>>(
            "inertia",
            {
                1e-5, 0,    0,
                0,    1e-5, 0,
                0,    0,    1e-5
            }
        ).data(),
        pnh.param<double>("mass", 1e-5)
    );
    pInertia = &inertia;

    pub = nh.advertise<geometry_msgs::WrenchStamped>("force", 1, true);
    ros::Subscriber sub = nh.subscribe("accel", 1, &accelCallback);

    ros::spin();
    return 0;
}