/******************************************************************************
 * Этот файл является частью проекта Assistant Space AutoPilot (ASAP).
 *
 * Проект Assistant Space AutoPilot (ASAP) распространяется согласно положениям
 * базовой модификации Омской открытой лицензии с указанием авторства.
 *
 * Copyright (c) 2023, Андрей Степанов <standmit@yandex.ru>
******************************************************************************/


/**
 * \file        src/wrench_publisher.cpp
 * \author      Андрей Степанов <standmit@yandex.ru>
 * \copyright   Омская открытая лицензия
 * \brief       Нода публикации тяги двигателя
 * \details
 * Тяга задаётся параметром \с ~value \c
*/


#include <ros/ros.h>
#include <geometry_msgs/WrenchStamped.h>


int main(int argc, char** argv) {
    ros::init(argc, argv, "wrench_publisher");
    ros::NodeHandle pnh("~");
    ros::Publisher pub = pnh.advertise<geometry_msgs::WrenchStamped>("/thrust", 1, true);
    geometry_msgs::WrenchStamped msg;
    pnh.getParam("frame", msg.header.frame_id);
    pnh.getParam("value", msg.wrench.force.x);
    ros::Rate rate(1.0);
    while (ros::ok()) {
        msg.header.stamp = ros::Time::now();
        pub.publish(msg);
        ros::spinOnce();
        rate.sleep();
    }
    return 0;
}