/******************************************************************************
 * Этот файл является частью проекта Assistant Space AutoPilot (ASAP).
 *
 * Проект Assistant Space AutoPilot (ASAP) распространяется согласно положениям
 * базовой модификации Омской открытой лицензии с указанием авторства.
 *
 * Copyright (c) 2023, Андрей Степанов <standmit@yandex.ru>
******************************************************************************/


/**
 * \file        src/pid.hpp
 * \author      Андрей Степанов <standmit@yandex.ru>
 * \copyright   Омская открытая лицензия
 * \brief       Описание модуля ПИД-регулятора
*/


#pragma once


#include <stdint.h>
#include <type_traits>


namespace ASAP {


/**
 * \brief   Отладочная информация ПИД-регулятора
*/
template <typename Scalar>
struct PIDInfo {
    Scalar act; ///< Текущее значение регулируемой величины
    Scalar tar; ///< Целевое значение регулируемой величины
    Scalar err; ///< Величина ошибки между целевым и текущим значением
    Scalar der; ///< Производная ошибки
    Scalar P;   ///< Отклик пропорциональной состовляющей
    Scalar I;   ///< Отклик интегральной состовляющей
    Scalar D;   ///< Отклик дифференциальной состовляющей
    Scalar out; ///< Управляющее воздействие

    typedef PIDInfo* Ptr;
};


/**
 * \brief   ПИД-регулятор
 * \tparam Scalar   Тип переменной для расчётов
*/
template <typename Scalar>
class PID {
    public:
        /**
         * \brief   Конструктор со стандартными значениями коэффициентов
        */
        PID();

        /**
         * \brief   Конструктор с конкретными значениями коэффициентов
         * \param P     Коэффициент пропорциональной состовляющей
         * \param I     Коэффициент интегральной состовляющей
         * \param D     Коэффициент дифференциальной состовляющей
        */
        PID(
            const Scalar P,
            const Scalar I,
            const Scalar D
        );

        PID(const PID&) = delete;

        PID& operator=(const PID&) = delete;

        ~PID() = default;

        /**
         * \brief   Расчёт управляющего воздействия
         * \tparam T    Тип внешних данных
         * \param timestamp     Отметка времени
         * \param target        Целевое значение регулируемой величины
         * \param value         Текущее значение регулируемой величины
         * \param info          Если не NULL, сюда записываются отладочные данные ПИД-регулятора
         * \return  Управляющее воздействие
        */
        template <typename T>
        Scalar operator()(
            const uint32_t timestamp,
            const T target,
            const T value,
            const typename PIDInfo<Scalar>::Ptr info = NULL
        );

        Scalar P;   ///< Коэффициент пропорциональной состовляющей
        Scalar I;   ///< Коэффициент интегральной состовляющей
        Scalar D;   ///< Коэффициент дифференциальной состовляющей

    private:
        PIDInfo<Scalar> info;       ///< Срез текущей информации
        uint32_t last_timestamp;    ///< Последняя отметка времени
        bool first_iteration;       ///< Флаг первой итерации
};


template <typename Scalar>
PID<Scalar>::PID():
        P(1.0),
        I(0.1),
        D(0.3),
        first_iteration(true)
{
    /*
      Поскольку отклик интегральной состовляющей всегда накапливается, в начале
      его нужно занулить.
    */
    info.I = 0;
}


template <typename Scalar>
PID<Scalar>::PID(
            const Scalar P_arg,
            const Scalar I_arg,
            const Scalar D_arg
):
        P(P_arg),
        I(I_arg),
        D(D_arg),
        first_iteration(true)
{
    /*
      Поскольку отклик интегральной состовляющей всегда накапливается, в начале
      его нужно занулить.
    */
    info.I = 0;
}


template <typename Scalar>
template <typename T>
Scalar PID<Scalar>::operator()(
        const uint32_t timestamp,
        const T target,
        const T value,
        const typename PIDInfo<Scalar>::Ptr info_out
) {
    // Вычисляем ошибку
    const auto error = target - value;

    // Вычисляем отклик пропорциональной состовляющей
    info.P = error * P;

    if (first_iteration) {
        /*
            Если это первая итерация, то производная ошибки и отклик
            дифференциальной состовляющей равны нулю.
        */
        info.D = 0;
        info.der = 0;
        first_iteration = false;
    } else {
        // Расчёт delta T с защитой от переполнения переменной
        const Scalar dt = timestamp > last_timestamp ?
            (timestamp - last_timestamp) :
            (
                std::numeric_limits<decltype(last_timestamp)>::max() -
                last_timestamp +
                timestamp -
                std::numeric_limits<
                    std::remove_const<
                        decltype(timestamp)
                    >::type
                >::min()
            );

        if (I > 1e-6)
            info.I += error * I * dt;
        else
            /*
                Если коэффициент интегральной состовляющей равен нулю,
                не производить вычисление её отклика. Из-за особенностей
                переменных с плавающей точкой даже при нулевом значении
                коэффициента может происходить накопление.
            */
            info.I = 0;

        // Расчёт производной ошибки
        info.der = (error - info.err) / dt;

        // Расчёт отклика дифференциальной состовляющей
        info.D = -info.der * D;
    }

    // Расчёт управляющего воздействия
    info.out = info.P + info.I + info.D;

    // Сохраненяем оставшуюся информацию
    info.err = error;
    last_timestamp = timestamp;
    info.tar = target;
    info.act = value;

    // Если запрошено, выдаём отладочную ифнормацию наружу
    if (info_out)
        *info_out = info;

    // Возвращаем управляющее воздействие
    return info.out;
}


}