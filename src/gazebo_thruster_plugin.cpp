/******************************************************************************
 * Этот файл является частью проекта Assistant Space AutoPilot (ASAP).
 *
 * Проект Assistant Space AutoPilot (ASAP) распространяется согласно положениям
 * базовой модификации Омской открытой лицензии с указанием авторства.
 *
 * Copyright (c) 2023, Андрей Степанов <standmit@yandex.ru>
******************************************************************************/


/**
 * \file        src/gazebo_thruster_plugin.cpp
 * \author      Андрей Степанов <standmit@yandex.ru>
 * \copyright   Омская открытая лицензия
 * \brief       Плагин реактивных двигателей для симулятора Gazebo
*/


#include <gazebo/common/common.hh>
#include <gazebo/common/Plugin.hh>
#include <gazebo/physics/Link.hh>
#include <gazebo/physics/Model.hh>
#include <ros/ros.h>
#include <geometry_msgs/WrenchStamped.h>
#include <forward_list>


namespace gazebo {


/**
 * \brief   Плагин реактивных двигателей
 * \details
 * Плагин позволяет установить на модель несколько реактивных двигателей.
 * Двигатели прикрепляются к конкретным links.
 * Максимальная тяга двигателя в Ньютонах задаётся в параметрах.
 * Уровень тяги 0..1 задаётся в процессе работы через топик.
*/
class SpaceThrustersPlugin : public ModelPlugin {
    public:
        void Load(
            physics::ModelPtr parent,
            sdf::ElementPtr sdf
        );

        void Update();
    
    protected:
        /**
         * \brief   Реактивный двигатель
        */
        class SpaceThruster {
            public:
                /**
                 * \param link          Link, который будет считаться двигателем
                 * \param max_thrust    Максимальная тяга двигателя (Н)
                 * \param nh            ros::NodeHandle для подписывания на топик
                */
                SpaceThruster(
                    const physics::LinkPtr link,
                    const float max_thrust,
                    ros::NodeHandle& nh
                );

                /**
                 * \brief   Приложить тягу двигателя к модели
                */
                void applyForce() const;

            private:
                /**
                 * \brief   Обработчик топика
                */
                void callback(const geometry_msgs::WrenchStamped& msg);

                const physics::LinkPtr link;            ///< Link, который считается двигателем
                const float max_thrust;                 ///< Максимальная тяга двигателя в Н
                ignition::math::Vector3d thrust_force;  ///< Текущий вектор тяги двигателя
                ros::Subscriber sub;                    ///< ros::Subscriber для топика
        };

        std::forward_list<SpaceThruster> thrusters;     ///< Список всех двигателей

        boost::shared_ptr<ros::NodeHandle> nh;          ///< ros::NodeHandle для чтения параметров

        event::ConnectionPtr update_connection;         ///< Необходим для обработки итераций симулятора
};


SpaceThrustersPlugin::SpaceThruster::SpaceThruster(
            const physics::LinkPtr link_arg,
            const float max_thrust_arg,
            ros::NodeHandle& nh
):
        link(link_arg),
        max_thrust(max_thrust_arg),
        thrust_force(decltype(thrust_force)::Zero),
        sub(nh.subscribe(link->GetName() + "/thrust_level", 1, &SpaceThruster::callback, this))
{}


void SpaceThrustersPlugin::SpaceThruster::callback(const geometry_msgs::WrenchStamped& msg) {
    auto thrust = msg.wrench.force.x;
    if (thrust < 0) {
        gzwarn << "Negative value in [" << sub.getTopic() << "] message. Clamp to 0";
        thrust = 0;
    } else if (thrust > 1) {
        gzwarn << "Overflow value in [" << sub.getTopic() << "] message. Clamp to 1";
        thrust = 1;
    }

    thrust_force.Set(thrust * max_thrust);
}


void SpaceThrustersPlugin::SpaceThruster::applyForce() const {
    link->AddLinkForce(thrust_force);
}


void SpaceThrustersPlugin::Load(
        physics::ModelPtr parent,
        sdf::ElementPtr sdf
) {
    if (!ros::isInitialized()) {
        gzerr << "A ROS node for Gazebo has not been initialized, unable to load plugin. "
        << "Load the Gazebo system plugin 'libgazebo_ros_api_plugin.so' in the gazebo_ros package" << std::endl;
        return;
    }

    {
        constexpr char robot_namespace_tag[] = "robotNamespace";
        GZ_ASSERT(sdf->HasElement(robot_namespace_tag), "ROS namespace for TheForce plugin is not specified");
        std::string ns = sdf->GetElement(robot_namespace_tag)->GetValue()->GetAsString();
        if ((not ns.empty()) && (ns.back() != '/'))
            ns += '/';
        ns += sdf->GetAttribute("name")->GetAsString().c_str();
        nh = boost::make_shared<ros::NodeHandle>(ns);
    }

    {
        constexpr char thrusters_tag[] = "thrusters";
        constexpr char max_thrust_attr[] = "max_thrust";
        GZ_ASSERT(sdf->HasElement(thrusters_tag), "Could not find thrusters list!");
        const auto thrusters_list = sdf->GetElement(thrusters_tag);
        auto thruster_item = thrusters_list->GetFirstElement();
        while (thruster_item) {
            const auto thruster_name = thruster_item->GetName();
            const auto link = parent->GetLink(thruster_name);
            GZ_ASSERT(link, "Could not find thruster link");
            GZ_ASSERT(
                thruster_item->HasAttribute(max_thrust_attr),
                "Could not find max_thrust"
            );
            const auto max_thrust_ptr = thruster_item->GetAttribute(max_thrust_attr);
            float max_thrust;
            const auto success = max_thrust_ptr->Get(max_thrust);
            GZ_ASSERT(
                success,
                "Failed to parse max_thrust"
            );
            thrusters.emplace_front(
                link,
                max_thrust,
                *nh
            );
            thruster_item = thruster_item->GetNextElement();
        }
    }

    update_connection = event::Events::ConnectWorldUpdateBegin(
        boost::bind(&SpaceThrustersPlugin::Update, this)
    );
}


void SpaceThrustersPlugin::Update() {
    for (const auto& thruster : thrusters)
        thruster.applyForce();
}


GZ_REGISTER_MODEL_PLUGIN(SpaceThrustersPlugin);


}