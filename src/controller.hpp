/******************************************************************************
 * Этот файл является частью проекта Assistant Space AutoPilot (ASAP).
 *
 * Проект Assistant Space AutoPilot (ASAP) распространяется согласно положениям
 * базовой модификации Омской открытой лицензии с указанием авторства.
 *
 * Copyright (c) 2023, Андрей Степанов <standmit@yandex.ru>
******************************************************************************/


/**
 * \file        src/controller.hpp
 * \author      Андрей Степанов <standmit@yandex.ru>
 * \copyright   Омская открытая лицензия
 * \brief       Описание модуля ПИД-контроллера
*/


#pragma once


#include "pid.hpp"


namespace ASAP {


template <typename T, uint8_t Dim>
using PIDInfoArray = PIDInfo<T>[Dim];


/**
 * \brief   ПИД-контроллер
 * \tparam T    Тип переменной для расчётов
 * \tparam Dims Количество контроллируемых степеней свободы
*/
template <typename T, uint8_t Dims>
class Controller {
    public:

        /**
         * \brief   Задать коэффициенты ПИД-регулятора
         * \param dim   Номер степени свободы, для которой задаются коэффициенты
         * \param P     Коэффициент пропорциональной состовляющей
         * \param I     Коэффициент интегральной состовляющей
         * \param D     Коэффициент дифференциальной состовляющей
        */
        void setPID(
            const uint8_t dim,
            const T P,
            const T I,
            const T D
        );

        /**
         * \brief   Рассчитать управляющее воздействие
         * \param[in] timestamp_ms  Отметка времени
         * \param[in] target        Массив целевых значений для всех степеней свободы
         * \param[in] pose          Массив текущих значений вдоль всех степеней свободы
         * \param[out] intention    Массив для записи управляющего воздействия
         * \param[out] info         Если не NULL, сюда будут записаны отладочные данные для каждого ПИД-регулятора
        */
        void getIntention(
            const uint32_t timestamp_ms,
            const T* const target,
            const T* const pose,
            T* const intention,
            PIDInfoArray<T, Dims>* const info = NULL
        );

    protected:
        PID<T> PIDs[Dims];  ///< ПИД-регуляторы для всех степеней свободы
};


template <typename T, uint8_t Dims>
void Controller<T, Dims>::setPID(
        const uint8_t dim,
        const T P,
        const T I,
        const T D
) {
    auto& pid = PIDs[dim];
    pid.P = P;
    pid.I = I;
    pid.D = D;
}


template <typename T, uint8_t Dims>
void Controller<T, Dims>::getIntention(
        const uint32_t timestamp_ms,
        const T* const target,
        const T* const pose,
        T* const intention,
        PIDInfoArray<T, Dims>* const info
) {
    for (uint8_t i = 0; i < Dims; i++)
        intention[i] = PIDs[i](
            timestamp_ms,
            target[i],
            pose[i],
            info ? (*info)[i] : NULL
        );
}


}