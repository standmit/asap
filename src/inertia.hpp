/******************************************************************************
 * Этот файл является частью проекта Assistant Space AutoPilot (ASAP).
 *
 * Проект Assistant Space AutoPilot (ASAP) распространяется согласно положениям
 * базовой модификации Омской открытой лицензии с указанием авторства.
 *
 * Copyright (c) 2023, Андрей Степанов <standmit@yandex.ru>
******************************************************************************/


/**
 * \file        src/inertia.hpp
 * \author      Андрей Степанов <standmit@yandex.ru>
 * \copyright   Омская открытая лицензия
 * \brief       Описание модуля пересчёта ускорения и силы
*/


#pragma once


#include <eigen3/Eigen/Eigen>


namespace ASAP {


/**
 * \brief   Модуль пересчёта ускорения и силы
 * \details
 * Имея информацию о массе и моменте инерции аппарата, модуль пересчитываеет
 * линейное и угловое ускорение в силу и моент инерции.
 * 
 * \tparam T    Тип данных для расчётов (float или double)
*/
template <typename Scalar>
class Inertia {
    public:
        Inertia() = delete;
        Inertia(const Inertia&) = delete;
        Inertia& operator=(const Inertia&) = delete;

        /**
         * \brief Базовый конструктор
         * \param inertia   Момент инерции в матричной форме (тензор инерции) (Row-major)
         * \param mass      Масса аппарата
        */
        template <typename TI, typename TM>
        Inertia(
            const TI* const inertia,
            const TM mass
        );

        ~Inertia() = default;

        /**
         * \brief   Пересчитать линейное ускорение в силу
         * \param[in] linear_accel  Вектор линейного ускорения { x, y, z }
         * \param[out] force        Вектор силы { x, y, z }
         * \details
         * \f$ \vec{F} = m\vec{a} \f$
        */
        template <typename TA, typename TF>
        void getForce(
            const TA* const linear_accel,
            TF* const force
        ) const;

        /**
         * \brief   Пересчитать угловое ускорение в крутящий момент
         * \param[in] angular_accel     Вектор углового ускорения { roll, pitch, yaw }
         * \param[out] torque           Крутящий момент { roll, pitch, yaw }
         * \details
         * \f$ \vec{M} = I\vec{\varepsilon } \f$
        */
        template <typename TA, typename TT>
        void getTorque(
            const TA* const angular_accel,
            TT* const torque
        ) const;

    private:
        const Eigen::Matrix<Scalar, 3, 3> inertia;
        const Scalar mass;
};


template <typename Scalar>
template <typename TI, typename TM>
Inertia<Scalar>::Inertia(
            const TI* const inertia_arg,
            const TM mass_arg
):
        inertia(
            Eigen::Map<const Eigen::Matrix<TI, 3, 3> >(
                inertia_arg
            ).template cast<Scalar>()
        ),
        mass(mass_arg)
{}


template <typename Scalar>
template <typename TA, typename TF>
void Inertia<Scalar>::getForce(
        const TA* const linear_accel,
        TF* const force
) const {
    (Eigen::Map<Eigen::Matrix<TF, 3, 1>>(force)) = (
        Eigen::Map<const Eigen::Matrix<TA, 3, 1>>(linear_accel) * mass
    ).template cast<TF>();
}


template <typename Scalar>
template <typename TA, typename TT>
void Inertia<Scalar>::getTorque(
        const TA* const angular_accel,
        TT* const torque
) const {
    (Eigen::Map<Eigen::Matrix<TT, 3, 1>>(torque)) = (
        inertia * Eigen::Map<const Eigen::Matrix<TA, 3, 1>>(angular_accel)
    ).template cast<TT>();
}


}