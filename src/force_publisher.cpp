/******************************************************************************
 * Этот файл является частью проекта Assistant Space AutoPilot (ASAP).
 *
 * Проект Assistant Space AutoPilot (ASAP) распространяется согласно положениям
 * базовой модификации Омской открытой лицензии с указанием авторства.
 *
 * Copyright (c) 2023, Андрей Степанов <standmit@yandex.ru>
******************************************************************************/

/**
 * \file        src/force_publisher.cpp
 * \author      Андрей Степанов <standmit@yandex.ru>
 * \copyright   Омская открытая лицензия
 * \brief       Нода публикации силы и крутящего момента
 * \details
 * Сила и крутящий момент задаются через dynamic_reconfigure
*/


#include <ros/ros.h>
#include <asap/ShipForceConfig.h>
#include <dynamic_reconfigure/server.h>
#include <geometry_msgs/WrenchStamped.h>


geometry_msgs::WrenchStamped msg;   ///< Сообщение для публикации


void reconfigre_cb(asap::ShipForceConfig& config, uint32_t) {
    msg.wrench.force.x = config.x;
    msg.wrench.force.y = config.y;
    msg.wrench.force.z = config.z;
    msg.wrench.torque.x = config.roll;
    msg.wrench.torque.y = config.pitch;
    msg.wrench.torque.z = config.yaw;
}


ros::Publisher pub;


void timer_cb(const ros::TimerEvent& te) {
    msg.header.stamp = te.current_real;
    pub.publish(msg);
}


int main(int argc, char** argv) {
    ros::init(argc, argv, "force_publisher");
    ros::NodeHandle nh;
    ros::NodeHandle pnh("~");
    pub = nh.advertise<geometry_msgs::WrenchStamped>("force", 1, true);
    msg.header.frame_id = pnh.param<std::string>("frame_id", "base_link");
    dynamic_reconfigure::Server<asap::ShipForceConfig> reconfigure_server(pnh);
    reconfigure_server.setCallback(&reconfigre_cb);
    ros::Timer pub_timer = pnh.createTimer(ros::Duration(ros::Rate(4)), &timer_cb);
    ros::spin();
    return 0;
}