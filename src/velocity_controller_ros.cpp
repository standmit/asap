/******************************************************************************
 * Этот файл является частью проекта Assistant Space AutoPilot (ASAP).
 *
 * Проект Assistant Space AutoPilot (ASAP) распространяется согласно положениям
 * базовой модификации Омской открытой лицензии с указанием авторства.
 *
 * Copyright (c) 2023, Андрей Степанов <standmit@yandex.ru>
******************************************************************************/


/**
 * \file        src/velocity_controller_ros.cpp
 * \author      Андрей Степанов <standmit@yandex.ru>
 * \copyright   Омская открытая лицензия
 * \brief       Нода контроллера скорости
*/


#include <ros/ros.h>
#include "controller.hpp"
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/AccelStamped.h>
#include <nav_msgs/Odometry.h>
#include <tf2_ros/transform_listener.h>
#include <message_filters/subscriber.h>
#include <tf2_ros/message_filter.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <asap/PIDConfig.h>
#include <asap/PIDTerms.h>
#include <dynamic_reconfigure/server.h>


/**
 * \brief   Преобразовать timestamp в милисекунды
*/
template <class T>
inline uint32_t toMs(const T& stamp) {
    return stamp.sec * 1000 + stamp.nsec / 1000000;
}


/**
 * \brief   Контроллер скорости
*/
class VelocityController : public ASAP::Controller<double, 6> {
    public:
        template <typename TargetM, typename PoseM, typename ResultM>
        void getIntention(
            const ros::Time timestamp,
            const TargetM& target,
            const PoseM& pose,
            ResultM& intention,
            ASAP::PIDInfoArray<double, 6>* const info = NULL
        );

    private:
        ros::Time start_stamp;
        bool first_iter = true;
};


template <typename TargetM, typename PoseM, typename ResultM>
void VelocityController::getIntention(
        const ros::Time timestamp,
        const TargetM& target,
        const PoseM& pose,
        ResultM& intention,
        ASAP::PIDInfoArray<double, 6>* const info
) {
    if (first_iter) {
        start_stamp = timestamp;
        first_iter = false;
    }
    const auto local_stamp = timestamp - start_stamp;
    const auto timestamp_ms = toMs(local_stamp);
    intention.linear.x = PIDs[0](
        timestamp_ms,
        target.linear.x,
        pose.linear.x,
        info ? &(*info)[0] : NULL
    );
    intention.linear.y = PIDs[1](
        timestamp_ms,
        target.linear.y,
        pose.linear.y,
        info ? &(*info)[1] : NULL
    );
    intention.linear.z = PIDs[2](
        timestamp_ms,
        target.linear.z,
        pose.linear.z,
        info ? &(*info)[2] : NULL
    );
    intention.angular.x = PIDs[3](
        timestamp_ms,
        target.angular.x,
        pose.angular.x,
        info ? &(*info)[3] : NULL
    );
    intention.angular.y = PIDs[4](
        timestamp_ms,
        target.angular.y,
        pose.angular.y,
        info ? &(*info)[4] : NULL
    );
    intention.angular.z = PIDs[5](
        timestamp_ms,
        target.angular.z,
        pose.angular.z,
        info ? &(*info)[5] : NULL
    );
}


namespace ASAP {


/**
 * \brief   Упаковать отладочную информацию ПИД-регулятора в сообщение asap::PIDTerms
*/
template <typename T>
asap::PIDTerms toMsg(const PIDInfo<T>& info) {
    asap::PIDTerms msg;
    msg.act = info.act;
    msg.tar = info.tar;
    msg.err = info.err;
    msg.der = info.der;
    msg.P = info.P;
    msg.I = info.I;
    msg.D = info.D;
    msg.out = info.out;
    return msg;
}


}


namespace tf2 {


template <>
inline
void doTransform(
        const geometry_msgs::Twist& t_in,
        geometry_msgs::Twist& t_out,
        const geometry_msgs::TransformStamped& transform
) {
    doTransform(t_in.linear, t_out.linear, transform);
    doTransform(t_in.angular, t_out.angular, transform);
}


template <>
inline
void doTransform(
        const geometry_msgs::TwistStamped& t_in,
        geometry_msgs::TwistStamped& t_out,
        const geometry_msgs::TransformStamped& transform
) {
    doTransform(t_in.twist, t_out.twist, transform);
    t_out.header.stamp = transform.header.stamp;
    t_out.header.frame_id = transform.header.frame_id;
}


template <>
inline
const ros::Time& getTimestamp(const geometry_msgs::TwistStamped& t) {
    return t.header.stamp;
}


template <>
inline
const std::string& getFrameId(const geometry_msgs::TwistStamped& t) {
    return t.header.frame_id;
}


}


VelocityController controller;  ///< Контроллер скорости
ros::Publisher pub;             ///< Публикация ускорения
geometry_msgs::AccelStamped accel_msg;
tf2_ros::Buffer tf_buffer;
ros::Duration period;
geometry_msgs::Twist target;    ///< Целевая скорость
bool debug;                     ///< Нужно ли выводить отладочную информацию
ros::Publisher x_pid_pub;       ///< Публикация отладочной информации ПИД-регуллятора по оси X
ros::Publisher y_pid_pub;       ///< Публикация отладочной информации ПИД-регуллятора по оси Y
ros::Publisher z_pid_pub;       ///< Публикация отладочной информации ПИД-регуллятора по оси Z
ros::Publisher roll_pid_pub;    ///< Публикация отладочной информации ПИД-регуллятора по оси Roll
ros::Publisher pitch_pid_pub;   ///< Публикация отладочной информации ПИД-регуллятора по оси Pitch
ros::Publisher yaw_pid_pub;     ///< Публикация отладочной информации ПИД-регуллятора по оси Yaw


void targetCallback(const geometry_msgs::TwistStamped& msg) {
    // Обновляем целевую скорость
    if (msg.header.frame_id == accel_msg.header.frame_id)
        target = msg.twist;
    else
        // Приводим к нужной системе координат, если необходимо
        tf2::doTransform(
            msg.twist,
            target,
            tf_buffer.lookupTransform(
                accel_msg.header.frame_id,
                msg.header.frame_id,
                msg.header.stamp
            )
        );
}


void poseCallback(nav_msgs::Odometry pose) {
    // Если необходимо, приводим текущую позицию к нужной системе координат
    if (pose.child_frame_id != accel_msg.header.frame_id)
        tf2::doTransform(
            pose.twist.twist,
            pose.twist.twist,
            tf_buffer.lookupTransform(
                accel_msg.header.frame_id,
                pose.child_frame_id,
                pose.header.stamp
            )
        );

    // Вычисляем управляющее воздействие (ускорение)
    if (debug) {
        // Запрашиваем и публикуем отладочную информацию, если требуется
        ASAP::PIDInfoArray<double, 6> info;
        controller.getIntention(
            pose.header.stamp,
            target,
            pose.twist.twist,
            accel_msg.accel,
            &info
        );
        x_pid_pub.publish(ASAP::toMsg(info[0]));
        y_pid_pub.publish(ASAP::toMsg(info[1]));
        z_pid_pub.publish(ASAP::toMsg(info[2]));
        roll_pid_pub.publish(ASAP::toMsg(info[3]));
        pitch_pid_pub.publish(ASAP::toMsg(info[4]));
        yaw_pid_pub.publish(ASAP::toMsg(info[5]));
    } else
        // Без отладочной информации
        controller.getIntention(
            pose.header.stamp,
            target,
            pose.twist.twist,
            accel_msg.accel
        );
    accel_msg.header.stamp = pose.header.stamp;
    pub.publish(accel_msg);
}


void reconfigure(const uint8_t dim, asap::PIDConfig& config) {
    controller.setPID(dim, config.P, config.I, config.D);
}


int main(int argc, char** argv) {
    ros::init(argc, argv, "velocity_controller");
    ros::NodeHandle nh("~");

    debug = nh.param("debug", false);

    // Регистрируем DynamicReconfigure для каждого ПИД-регулятора
    ros::NodeHandle nh_X(nh, "axis_x");
    dynamic_reconfigure::Server<asap::PIDConfig> axis_x_server(nh_X);
    axis_x_server.setCallback(boost::bind(&reconfigure, 0, _1));
    ros::NodeHandle nh_Y(nh, "axis_y");
    dynamic_reconfigure::Server<asap::PIDConfig> axis_y_server(nh_Y);
    axis_y_server.setCallback(boost::bind(&reconfigure, 1, _1));
    ros::NodeHandle nh_Z(nh, "axis_z");
    dynamic_reconfigure::Server<asap::PIDConfig> axis_z_server(nh_Z);
    axis_z_server.setCallback(boost::bind(&reconfigure, 2, _1));
    ros::NodeHandle nh_Roll(nh, "axis_roll");
    dynamic_reconfigure::Server<asap::PIDConfig> axis_roll_server(nh_Roll);
    axis_roll_server.setCallback(boost::bind(&reconfigure, 3, _1));
    ros::NodeHandle nh_Pitch(nh, "axis_pitch");
    dynamic_reconfigure::Server<asap::PIDConfig> axis_pitch_server(nh_Pitch);
    axis_pitch_server.setCallback(boost::bind(&reconfigure, 4, _1));
    ros::NodeHandle nh_Yaw(nh, "axis_yaw");
    dynamic_reconfigure::Server<asap::PIDConfig> axis_yaw_server(nh_Yaw);
    axis_yaw_server.setCallback(boost::bind(&reconfigure, 5, _1));

    if (debug) {
        x_pid_pub     = nh_X.advertise<asap::PIDTerms>("pid_terms", 1, false);
        y_pid_pub     = nh_Y.advertise<asap::PIDTerms>("pid_terms", 1, false);
        z_pid_pub     = nh_Z.advertise<asap::PIDTerms>("pid_terms", 1, false);
        roll_pid_pub  = nh_Roll.advertise<asap::PIDTerms>("pid_terms", 1, false);
        pitch_pid_pub = nh_Pitch.advertise<asap::PIDTerms>("pid_terms", 1, false);
        yaw_pid_pub   = nh_Yaw.advertise<asap::PIDTerms>("pid_terms", 1, false);
    }

    accel_msg.header.frame_id = nh.param<std::string>("frame", "base_link");
    pub = nh.advertise<geometry_msgs::AccelStamped>("acceleration", 1);
    tf2_ros::TransformListener tf_listener(tf_buffer);

    /*
        Регистрируем подписку на целевое и текущее значение скорости
        с фильтрацией по доступности преобразования TF
    */
    message_filters::Subscriber<geometry_msgs::TwistStamped> target_sub(
        nh,
        "velocity",
        1
    );
    tf2_ros::MessageFilter<geometry_msgs::TwistStamped> target_filter(
        target_sub,
        tf_buffer,
        accel_msg.header.frame_id,
        1,
        nh
    );
    target_filter.registerCallback(&targetCallback);
    message_filters::Subscriber<nav_msgs::Odometry> pose_sub(
        nh,
        "pose",
        1
    );
    tf2_ros::MessageFilter<nav_msgs::Odometry> pose_filter(
        pose_sub,
        tf_buffer,
        accel_msg.header.frame_id,
        1,
        nh
    );
    pose_filter.registerCallback(&poseCallback);

    ros::spin();

    return 0;
}