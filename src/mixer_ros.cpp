/******************************************************************************
 * Этот файл является частью проекта Assistant Space AutoPilot (ASAP).
 *
 * Проект Assistant Space AutoPilot (ASAP) распространяется согласно положениям
 * базовой модификации Омской открытой лицензии с указанием авторства.
 *
 * Copyright (c) 2023, Андрей Степанов <standmit@yandex.ru>
******************************************************************************/


/**
 * \file        src/mixer_ros.cpp
 * \author      Андрей Степанов <standmit@yandex.ru>
 * \copyright   Омская открытая лицензия
 * \brief       Нода миксера
*/


#include "mixer.hpp"
#include <ros/ros.h>
#include <geometry_msgs/WrenchStamped.h>


std::vector<std::pair<ros::Publisher, geometry_msgs::WrenchStamped>> pubs;
ASAP::Mixer<double> mixer;


void forceCallback(const geometry_msgs::WrenchStamped& force) {
    std::vector<double> thrusts(pubs.size());
    mixer.getThrust(
        std::array<double, 6>{
            force.wrench.force.x,
            force.wrench.force.y,
            force.wrench.force.z,
            force.wrench.torque.x,
            force.wrench.torque.y,
            force.wrench.torque.z
        }.data(),
        thrusts.data()
    );
    auto thrust = thrusts.cbegin();
    auto thruster = pubs.begin();
    while (thrust != thrusts.cend()) {
        auto& msg = thruster->second;
        msg.header.stamp = force.header.stamp;
        msg.wrench.force.x = *thrust;
        thruster->first.publish(msg);
        thrust++;
        thruster++;
    }
}


template <typename T>
inline
T getNumber(const XmlRpc::XmlRpcValue& xml) {
    const auto& xml_type = xml.getType();
    return (xml_type == XmlRpc::XmlRpcValue::TypeDouble) ?
            static_cast<const double>(xml) :
            static_cast<const int>(xml);
}


int main(int argc, char** argv) {
    ros::init(argc, argv, "mixer");
    ros::NodeHandle nh;
    ros::NodeHandle pnh("~");
    ros::NodeHandle tnh(pnh,"thrusters");

    {
        XmlRpc::XmlRpcValue thrusters;
        if (not pnh.getParam("thrusters", thrusters)) {
            ROS_FATAL("Can't read thrusters configuration");
            return 1;
        }
        ROS_ASSERT(thrusters.getType() == XmlRpc::XmlRpcValue::TypeStruct);
        pubs.reserve(thrusters.size());
        for (const auto thruster : thrusters) {
            ROS_ASSERT(thruster.second.hasMember("pose"));
            const auto& pose = thruster.second["pose"];
            ROS_ASSERT(pose.getType() == XmlRpc::XmlRpcValue::TypeArray);
            double pose_array[6];
            for (uint8_t i = 0; i < 6; i++) {
                const auto& coord = pose[i];
                const auto& coord_type = coord.getType();
                ROS_ASSERT(
                    coord_type == XmlRpc::XmlRpcValue::TypeDouble
                    ||
                    coord_type == XmlRpc::XmlRpcValue::TypeInt
                );
                pose_array[i] = getNumber<double>(coord);
            }
            mixer.addThruster(pose_array);
            {
                geometry_msgs::WrenchStamped msg;
                msg.header.frame_id = thruster.first;
                pubs.emplace_back(
                    tnh.advertise<geometry_msgs::WrenchStamped>(thruster.first + "/thrust_level", 1, true),
                    msg
                );
            }
        }
    }

    ros::Subscriber sub = nh.subscribe("force", 1, &forceCallback);

    ros::spin();

    return 0;
}