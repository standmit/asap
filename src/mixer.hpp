/******************************************************************************
 * Этот файл является частью проекта Assistant Space AutoPilot (ASAP).
 *
 * Проект Assistant Space AutoPilot (ASAP) распространяется согласно положениям
 * базовой модификации Омской открытой лицензии с указанием авторства.
 *
 * Copyright (c) 2023, Андрей Степанов <standmit@yandex.ru>
******************************************************************************/


/**
 * \file        src/mixer.hpp
 * \author      Андрей Степанов <standmit@yandex.ru>
 * \copyright   Омская открытая лицензия
 * \brief       Описание модуля миксера
*/


#pragma once


#include <eigen3/Eigen/Eigen>
#include "transformations.hpp"


namespace ASAP {


/**
 * \brief   Миксер двигателей
 * \details
 * На основе информации о конфигурации двигателей аппарата распределяет тягу
 * между двигателями для обеспечения требуемой линейной тяги и крутящего
 * момента.
 * 
 * Для разложения векторя тяги и крутящего момента на значения тяги каждого
 * двигателя строится матрица \f$ M \f$ размером \f$ (N \times 6) \f$,
 * где \f$ N \f$ - количество двигателей.
 * Каждый столбец матрицы соотвествует одной степени свободы, по которой
 * может двигаться аппарат.
 * 
 * Столбцы идут в следующем порядке:
 * 1. X
 * 2. Y
 * 3. Z
 * 4. Roll
 * 5. Pitch
 * 6. Yaw
 * 
 * Таким образом, левая половина каждой строки содержит разложение единичного
 * вектора тяги одного двигателя по трём осям для линейного перемещения,
 * а правая половина — для вращательного движения.
 * Требуемые значения тяги всех двигателей получаются путём умножения
 * матрицы \f$ M \f$ на вектор силы и крутящего момента.
 * 
 * \f[ \vec{Thrust} = M \cdot \vec{W} \f]
 * 
 * \tparam T    тип данных для расчётов (float или double)
*/
template <typename Scalar>
class Mixer {
    public:
        Mixer() = default;

        Mixer(const Mixer&) = delete;
        Mixer& operator=(const Mixer&) = delete;

        ~Mixer() = default;

        /**
         * \brief   Добавить двигатель в конфигурацию
         *
         *  \details
         * В конец матрицы добавляется новая строка \f$ \begin{pmatrix}
         * M_{X\,i} && M_{Y\,i} && M_{Z\,i} && M_{Roll\,i} && M_{Pitch\,i} &&
         * M_{Yaw\,i} \end{pmatrix} \f$.
         * 
         * \f[ \begin{pmatrix} M_{X\,i} \\ M_{Y\,i} \\ M_{Z\,i} \end{pmatrix} =
         * R_i \cdot \begin{pmatrix} 1 \\ 0 \\ 0 \end{pmatrix} \f]
         * 
         * \f$ R_i \f$ — матрица ориентации (вращения) \f$ i \f$-го двигателя
         * 
         * \param pose  Позиция двигателя относительно центра масс аппарата
         *              (X, Y, Z, Roll, Pitch, Yaw)
         */
        template <typename TP>
        void addThruster(const TP* const pose);

        /**
         * \brief   Рассчитать тягу двигателей для создания данной силы и
         * крутящего момента
         * 
         * \details
         * Тяга рассчитывается путём умножения матрицы \f$ M \f$ на вектор
         * силы и крутящего момента \f$ \vec{W} \f$.
         * 
         * \f[ \vec{Thrust} = M \cdot \vec{W} \f]
         * 
         * \f[ \begin{matrix} \begin{matrix} \\ Thrust \\ \begin{pmatrix}
         * T_1 \\ T_2 \\ ... \\ T_N \end{pmatrix} \end{matrix} &&
         * \begin{matrix} \\ \\ \begin{matrix} \leftarrow \\ \leftarrow \\ \\
         * \leftarrow \end{matrix} \end{matrix} && \begin{matrix} \left( X
         * \right. && Y && Z && Roll && Pitch && \left. Yaw \right) \\
         * \downarrow && \downarrow && \downarrow && \downarrow && \downarrow
         * && \downarrow \\ \left( \begin{matrix} M_{X1} \\ M_{X2} \\ ... \\
         * M_{XN} \end{matrix} \right. && \begin{matrix} M_{Y1} \\ M_{Y2} \\
         * ... \\ M_{YN} \end{matrix} && \begin{matrix} M_{Z1} \\ M_{Z2} \\ ...
         * \\ M_{ZN} \end{matrix} && \begin{matrix} M_{Roll1} \\ M_{Roll2} \\
         * ... \\ M_{RollN} \end{matrix} && \begin{matrix} M_{Pitch1} \\
         * M_{Pitch2} \\ ... \\ M_{PitchN} \end{matrix} && \left.
         * \begin{matrix} M_{Yaw1} \\ M_{Yaw2} \\ ... \\ M_{YawN} \end{matrix}
         * \right) \end{matrix} \end{matrix} \f]
         * 
         * \f[ T_i = M_{X\,i}\,X + M_{Y\,i}\,Y + M_{Z\,i}\,Z +
         * M_{Roll\,i}\,Roll + M_{Pitch\,i}\,Pitch + M_{Yaw\,i}\,Yaw \f]
         * 
         * \param[in] wrench    Вектор \f$ \vec{W} \f$ силы и крутящего
         * момента
         * \param[out] thrust   Вектор тяги всех двигателей. Коэффициенты
         * располагаются в том же порядке, в котором двигатели были добавлены
         * в миксер методом addThruster.
         */
        template <typename TW, typename TT>
        void getThrust(
            const TW* const wrench,
            TT* const thrust
        ) const;

    private:
        Eigen::Matrix<Scalar, Eigen::Dynamic, 6> matrix;
};


template <typename Scalar>
template <typename TP>
void Mixer<Scalar>::addThruster(const TP* const pose) {
    matrix.conservativeResize(matrix.rows() + 1, Eigen::NoChange);
    auto row = matrix.template bottomRows<1>();
    const auto orientation = makeQuat<Scalar>(pose[3], pose[4], pose[5]);
    auto trans_block = row.template block<1, 3>(0, 0);
    trans_block = (orientation * Eigen::Matrix<Scalar, 3, 1>::UnitX()).transpose();
    {
        const auto absX = std::abs(pose[0]);
        const auto absY = std::abs(pose[1]);
        const auto absZ = std::abs(pose[2]);
        const auto legX = pose[0] / absX;
        const auto legY = pose[1] / absY;
        const auto legZ = pose[2] / absZ;
        auto rot_block = row.template block<1, 3>(0, 3);
        rot_block = Eigen::Matrix<Scalar, 1, 3>{
            ((absY < Scalar(1e-3)) ? Scalar(0) : (legY * trans_block(0,2)        )) +
            ((absZ < Scalar(1e-3)) ? Scalar(0) : (legZ * trans_block(0,1) * Scalar(-1))),
            ((absX < Scalar(1e-3)) ? Scalar(0) : (legX * trans_block(0,2) * Scalar(-1))) +
            ((absZ < Scalar(1e-3)) ? Scalar(0) : (legZ * trans_block(0,0)        )),
            ((absX < Scalar(1e-3)) ? Scalar(0) : (legX * trans_block(0,1)        )) +
            ((absY < Scalar(1e-3)) ? Scalar(0) : (legY * trans_block(0,0) * Scalar(-1)))
        };
        rot_block.normalize();
    }
    trans_block.normalize();
}


template <typename Scalar>
template <typename TW, typename TT>
void Mixer<Scalar>::getThrust(
        const TW* const wrench,
        TT* const thrust
) const {
    const auto desired_thrust = matrix * Eigen::Map<const Eigen::Matrix<TW, 6, 1>>(wrench);
    const auto zero_clamped_thrust = (desired_thrust.array() > 0).select(desired_thrust, 0);    // put zero to negative elements
    const auto max_clamped_thrust = (zero_clamped_thrust.array() > 1).select(1, zero_clamped_thrust);  // limit max thrust
    Eigen::Map<Eigen::Matrix<TT, Eigen::Dynamic, 1>>(thrust, matrix.rows()) = max_clamped_thrust;
}


}