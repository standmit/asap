/******************************************************************************
 * Этот файл является частью проекта Assistant Space AutoPilot (ASAP).
 *
 * Проект Assistant Space AutoPilot (ASAP) распространяется согласно положениям
 * базовой модификации Омской открытой лицензии с указанием авторства.
 *
 * Copyright (c) 2023, Андрей Степанов <standmit@yandex.ru>
******************************************************************************/


/**
 * \file        src/gazebo_thruster_plugin.cpp
 * \author      Андрей Степанов <standmit@yandex.ru>
 * \copyright   Омская открытая лицензия
 * \brief       Плагин реактивных двигателей для симулятора Gazebo
*/


#include <ros/ros.h>
#include <gazebo_msgs/ModelStates.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <nav_msgs/Odometry.h>
#include <tf2_ros/transform_broadcaster.h>


ros::Duration min_period;                               ///< Минимальный период, с которым необходимо публиковать позицию
std::string model_name;                                 ///< Имя link в симуляторе
nav_msgs::Odometry odom;                                ///< Сообщение Odometry для публикации
ros::Publisher pub;                                     ///< для публикации Odometry
tf2_ros::TransformBroadcaster* tf_broadcaster_ptr;      ///< для публикации tf
geometry_msgs::TransformStamped tf_msg;                 ///< Сообщение TransformStamped для публикации


namespace tf2 {


/**
 * \details
 * В пакете tf2_geometry_msgs не оказалось специализации doTransform
 * для сообщения geometry_msgs/Twist.
 * Пришлось добавить здесь.
*/
template <>
void doTransform(
        const geometry_msgs::Twist& data_in,
        geometry_msgs::Twist& data_out,
        const geometry_msgs::TransformStamped& transform
) {
    tf2::doTransform(data_in.linear, data_out.linear, transform);
    tf2::doTransform(data_in.angular, data_out.angular, transform);
}


}


/**
 * \brief   Обработчик сообщений от симулятора
*/
void states_cb(const gazebo_msgs::ModelStates& msg) {
    const ros::Time stamp = ros::Time::now();
    // соблюдаем минимальный период
    if ((stamp - odom.header.stamp) < min_period)
        return;

    const auto model_it = std::find(
        msg.name.cbegin(),
        msg.name.cend(),
        model_name
    );

    if (model_it == msg.name.cend()) {
        ROS_ERROR_THROTTLE(
            1,
            "Can't find '%s' model!",
            model_name.c_str()
        );
        return;
    }
    
    const auto model_idx = std::distance(msg.name.cbegin(), model_it);
    const auto& pose = *(msg.pose.cbegin() + model_idx);
    const auto& global_twist = *(msg.twist.cbegin() + model_idx);
    
    tf_msg.header.stamp = stamp;
    tf_msg.transform.translation.x = pose.position.x;
    tf_msg.transform.translation.y = pose.position.y;
    tf_msg.transform.translation.z = pose.position.z;
    tf_msg.transform.rotation = pose.orientation;
    tf_broadcaster_ptr->sendTransform(tf_msg);

    odom.header.stamp = stamp;
    odom.pose.pose = pose;
    {
        /*
            Симулятор публикует скорость объекта в глобальной системе
            координат. В сообщении nav_msgs/Odometry скорость должна
            быть в локальной системе координат. Переводим.
        */
        tf2::Transform tf;
        tf2::convert(tf_msg.transform, tf);
        tf2::convert(tf.inverse(), tf_msg.transform);
        tf2::doTransform(global_twist, odom.twist.twist, tf_msg);
    }
    pub.publish(odom);
}


int main(int argc, char** argv) {
    ros::init(argc, argv, "gazebo_groundtruth");
    ros::NodeHandle pnh("~");
    
    min_period = ros::Duration(
        ros::Rate(
            pnh.param("max_rate", 60.0)
        )
    );
    
    if (not pnh.getParam("model_name", model_name)) {
        ROS_FATAL("Set model_name parameter!");
        return 1;
    }

    odom.header.frame_id = "world";
    odom.child_frame_id = pnh.param<std::string>("frame_id", "groudntruth");

    tf_msg.header.frame_id = odom.header.frame_id;
    tf_msg.child_frame_id = odom.child_frame_id;

    pub = pnh.advertise<nav_msgs::Odometry>("odom", 10);

    tf2_ros::TransformBroadcaster tf_broadcaster;
    tf_broadcaster_ptr = &tf_broadcaster;

    ros::Subscriber sub = pnh.subscribe("/gazebo/link_states", 60, &states_cb);

    ros::spin();

    return 0;   
}