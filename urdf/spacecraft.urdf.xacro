<?xml version="1.0"?>

<!--
Этот файл является частью проекта Assistant Space AutoPilot (ASAP).

Проект Assistant Space AutoPilot (ASAP) распространяется согласно положениям
базовой модификации Омской открытой лицензии с указанием авторства.

Copyright (c) 2023, Андрей Степанов <standmit@yandex.ru>
-->

<robot
        name="spacecraft"
        xmlns:xacro="http://www.ros.org/wiki/xacro"
>
    <link name="base_link" />
    <link name="body">
        <inertial>
            <mass value="3" />
            <inertia
                ixx="0.5"
                ixy="0"
                ixz="0"
                iyy="0.5"
                iyz="0"
                izz="0.5"
            />
        </inertial>
        <visual>
            <geometry>
                <box size="4 2.4 2.4" />
            </geometry>
            <material name="grey">
                <color rgba="0.8 0.8 0.8 1" />
            </material>
        </visual>
    </link>
    <gazebo reference="body">
        <material>Gazebo/LightGrey</material>
    </gazebo>
    <joint name="body_joint" type="fixed">
        <parent link="base_link" />
        <child link="body" />
    </joint>
    <xacro:macro name="thruster" params="name *origin">
        <link name="${name}">
            <inertial>
                <mass value="1e-5" />
                <inertia
                    ixx="1e-5"
                    ixy="0"
                    ixz="0"
                    iyy="1e-5"
                    iyz="0"
                    izz="1e-5"
                />
            </inertial>
            <visual>
                <geometry>
                    <cylinder radius="0.2" length="0.5" />
                </geometry>
                <origin xyz="-0.25 0 0" rpy="0 ${pi/2} 0" />
                <material name="red_transp">
                    <color rgba="1 0 0 0.4" />
                </material>
            </visual>
        </link>
        <gazebo reference="${name}">
            <material>Gazebo/Red</material>
        </gazebo>
        <joint name="${name}_joint" type="fixed">
            <parent link="body" />
            <child link="${name}" />
            <xacro:insert_block name="origin" />
        </joint>
        <gazebo reference="${name}_joint">
            <preserveFixedJoint>true</preserveFixedJoint>
        </gazebo>
    </xacro:macro>
    <xacro:thruster name="back_right_thruster">
        <origin xyz="-2 -1 0" />
    </xacro:thruster>
    <xacro:thruster name="back_left_thruster">
        <origin xyz="-2 1 0" />
    </xacro:thruster>
    <xacro:thruster name="front_right_thruster">
        <origin xyz="2 -1 0" rpy="0 0 ${pi}" />
    </xacro:thruster>
    <xacro:thruster name="front_left_thruster">
        <origin xyz="2 1 0" rpy="0 0 ${pi}" />
    </xacro:thruster>
    <xacro:thruster name="right_back_bottom_thruster">
        <origin xyz="-1 -1.2 -1" rpy="0 0 ${pi/2}" />
    </xacro:thruster>
    <xacro:thruster name="right_back_top_thruster">
        <origin xyz="-1 -1.2 1" rpy="0 0 ${pi/2}" />
    </xacro:thruster>
    <xacro:thruster name="right_front_bottom_thruster">
        <origin xyz="1 -1.2 -1" rpy="0 0 ${pi/2}" />
    </xacro:thruster>
    <xacro:thruster name="right_front_top_thruster">
        <origin xyz="1 -1.2 1" rpy="0 0 ${pi/2}" />
    </xacro:thruster>
    <xacro:thruster name="left_back_bottom_thruster">
        <origin xyz="-1 1.2 -1" rpy="0 0 ${-pi/2}" />
    </xacro:thruster>
    <xacro:thruster name="left_back_top_thruster">
        <origin xyz="-1 1.2 1" rpy="0 0 ${-pi/2}" />
    </xacro:thruster>
    <xacro:thruster name="left_front_bottom_thruster">
        <origin xyz="1 1.2 -1" rpy="0 0 ${-pi/2}" />
    </xacro:thruster>
    <xacro:thruster name="left_front_top_thruster">
        <origin xyz="1 1.2 1" rpy="0 0 ${-pi/2}" />
    </xacro:thruster>
    <xacro:thruster name="bottom_right_back_thruster">
        <origin xyz="-1 -1 -1.2" rpy="0 ${-pi/2} 0" />
    </xacro:thruster>
    <xacro:thruster name="bottom_left_back_thruster">
        <origin xyz="-1 1 -1.2" rpy="0 ${-pi/2} 0" />
    </xacro:thruster>
    <xacro:thruster name="bottom_right_front_thruster">
        <origin xyz="1 -1 -1.2" rpy="0 ${-pi/2} 0" />
    </xacro:thruster>
    <xacro:thruster name="bottom_left_front_thruster">
        <origin xyz="1 1 -1.2" rpy="0 ${-pi/2} 0" />
    </xacro:thruster>
    <xacro:thruster name="top_right_back_thruster">
        <origin xyz="-1 -1 1.2" rpy="0 ${pi/2} 0" />
    </xacro:thruster>
    <xacro:thruster name="top_left_back_thruster">
        <origin xyz="-1 1 1.2" rpy="0 ${pi/2} 0" />
    </xacro:thruster>
    <xacro:thruster name="top_right_front_thruster">
        <origin xyz="1 -1 1.2" rpy="0 ${pi/2} 0" />
    </xacro:thruster>
    <xacro:thruster name="top_left_front_thruster">
        <origin xyz="1 1 1.2" rpy="0 ${pi/2} 0" />
    </xacro:thruster>
    <gazebo>
        <plugin name="thrusters" filename="libspacethruster.so">
            <thrusters>
                <back_right_thruster max_thrust="10" />
                <back_left_thruster max_thrust="10" />
                <front_right_thruster max_thrust="10" />
                <front_left_thruster max_thrust="10" />
                <right_back_bottom_thruster max_thrust="10" />
                <right_back_top_thruster max_thrust="10" />
                <right_front_bottom_thruster max_thrust="10" />
                <right_front_top_thruster max_thrust="10" />
                <left_back_bottom_thruster max_thrust="10" />
                <left_back_top_thruster max_thrust="10" />
                <left_front_bottom_thruster max_thrust="10" />
                <left_front_top_thruster max_thrust="10" />
                <bottom_right_back_thruster max_thrust="10" />
                <bottom_left_back_thruster max_thrust="10" />
                <bottom_right_front_thruster max_thrust="10" />
                <bottom_left_front_thruster max_thrust="10" />
                <top_right_back_thruster max_thrust="10" />
                <top_left_back_thruster max_thrust="10" />
                <top_right_front_thruster max_thrust="10" />
                <top_left_front_thruster max_thrust="10" />
            </thrusters>
            <robotNamespace>spacecraft</robotNamespace>
        </plugin>
    </gazebo>
</robot>