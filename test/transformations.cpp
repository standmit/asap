/******************************************************************************
 * Этот файл является частью проекта Assistant Space AutoPilot (ASAP).
 *
 * Проект Assistant Space AutoPilot (ASAP) распространяется согласно положениям
 * базовой модификации Омской открытой лицензии с указанием авторства.
 *
 * Copyright (c) 2023, Андрей Степанов <standmit@yandex.ru>
******************************************************************************/


/**
 * \file        test/transformations.cpp
 * \author      Андрей Степанов <standmit@yandex.ru>
 * \copyright   Омская открытая лицензия
 * \brief       Тесты модуля преобразования координат
*/


#include <gtest/gtest.h>
#include "transformations.hpp"
#include <tf2/LinearMath/Quaternion.h>


/**
 * \brief   Получить случайное значение
 * \param max_value     Верхняя граница диапазона случайоного значения
*/
template <typename T>
inline
T rand_value(const T max_value = std::numeric_limits<T>::max()) {
    return static_cast<T>(std::rand()) / RAND_MAX * max_value;
}


/**
 * \brief   Получить случайно значение в заданной области
 * \param mean  Математическое ожидание заданной области
 * \param ampl  Максимальное отклонение (половина ширины заданной области)
*/
template <typename T>
inline
T rand_value(const T mean, const T ampl) {
    return mean + rand_value<T>(2 * ampl) - ampl;
}


/**
 * \test   Тест преобразования углов в кватернион
*/
TEST(Transformations, RPY_to_Quat) {
    const double roll = rand_value(0.0, M_PI);
    const double pitch = rand_value(0.0, M_PI);
    const double yaw = rand_value(0.0, M_PI);

    const auto eigen_quat = ASAP::makeQuat<double>(roll, pitch, yaw);
    tf2::Quaternion tf_quat;
    tf_quat.setRPY(roll, pitch, yaw);

    EXPECT_NEAR(eigen_quat.x(), tf_quat.x(), 1e-3);
    EXPECT_NEAR(eigen_quat.y(), tf_quat.y(), 1e-3);
    EXPECT_NEAR(eigen_quat.z(), tf_quat.z(), 1e-3);
    EXPECT_NEAR(eigen_quat.w(), tf_quat.w(), 1e-3);
}


TEST(Transformations, VectorRotaion) {
    const double x = rand_value(0.0, 1.0);
    const double y = rand_value(0.0, 1.0);
    const double z = rand_value(0.0, 1.0);
    const double roll = rand_value(0.0, M_PI);
    const double pitch = rand_value(0.0, M_PI);
    const double yaw = rand_value(0.0, M_PI);

    const auto eigen_quat = ASAP::makeQuat<double>(roll, pitch, yaw);
    const auto eigen_vec = eigen_quat.inverse() * Eigen::Vector3d(x, y, z);

    tf2::Quaternion tf_quat;
    tf_quat.setRPY(roll, pitch, yaw);
    const auto tf_vec = tf2::quatRotate(tf_quat.inverse(), tf2::Vector3(x, y, z));

    EXPECT_NEAR(eigen_vec.x(), tf_vec.x(), 1e-3);
    EXPECT_NEAR(eigen_vec.y(), tf_vec.y(), 1e-3);
    EXPECT_NEAR(eigen_vec.z(), tf_vec.z(), 1e-3);
}


int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);

    std::srand(std::time(nullptr));

    return RUN_ALL_TESTS();
}